import collections
from collections import OrderedDict
from datetime import datetime, date
from http.client import HTTPResponse
from json import loads, dumps

try:
    from typing import Union, Optional, Dict, Sequence, Iterable, Mapping
except ImportError:
    Union = Optional = Dict = Sequence = Iterable = Mapping

import re

from decimal import Decimal

from gladson.replication.errors import GladsonReplicationAPIKeyWarning


def str2date(s: str):
    """
    Parses a Gladson replication API date-string and return a python `date` object.

    :param s: A text representation of a date object.
    :return: A python date object

    >>> str2date('11/03/2016')
    date(2016, 11, 3)

    >>> str2date('2016/11/03')
    date(2016, 11, 3)
    """
    try:
        dt = datetime.strptime(s, '%Y-%m-%d')
        return date(dt.year, dt.month, dt.day)
    except ValueError:
        try:
            dt = datetime.strptime(s, '%Y/%m/%d')
            return date(dt.year, dt.month, dt.day)
        except ValueError:
            dt = datetime.strptime(s, '%m/%d/%Y')
    return date(dt.year, dt.month, dt.day)


class JSONArray(list):
    """
    An array for use with Gladson Replication REST API (JSON) response objects. Casting a `JSONArray` as a string
    will produce a JSON-compatible string in a predictable format.
    """

    def __init__(
        self,
        items,  # type: Sequence
        response=None  # type: HTTPResponse
    ):
        """
        In child classes, initialization should accept arguments matching the JSON object's
        properties, and should assign these values *explicitly* to attributes of the same name.
        """
        super().__init__(items)
        self.response = response

    @property
    def data(self):
        # type: ()-> list
        """
        :return: A list of values campatible with JSON serialization.
        """
        l = []
        for v in self:
            if v is None:
                continue
            if isinstance(v, (JSONArray, JSONObject)):
                v = v.data
            elif isinstance(v, collections.Sequence) and not isinstance(v, (str, bytes)):
                v = JSONArray(v).data
            #elif isinstance(v, Dict) and not isinstance(v, (str, bytes)):
            #    v = JSONObject(v).data
            elif isinstance(v, date):
                v = v.strftime('%m/%d/%Y')
            elif isinstance(v, datetime):
                v = v.strftime('%Y-%m-%dT%H:%M:%S%z')
            l.append(v)
        return l

    def __str__(self):
        # type: () -> str
        """
        :return: A JSON representation of this array.
        """
        return dumps(self.data)

    def __repr__(self):
        # type: () -> str
        """
        :return: A text representation of this array suitable for evaluating in Python.
        """
        r = '[%s]' % (
            '\n    ' + ',\n    '.join(
                (
                    '\n\t'.join(repr(v).split('\n')) if isinstance(v, JSONObject)
                    else re.sub(
                        r'(\r?\n)\s*',
                        r'',
                        re.sub(
                            r'(,\r?\n)\s*',
                            r', ',
                            repr(v)
                        )
                    )
                )
                for v in self
            ) + '\n'
            if self
            else ''
        )
        cn = self.__class__.__name__.split('.')[-1]
        if cn != 'JSONArray':
            r = '%s(%s)' % (cn, r)
        return r

    def __eq__(self, other):
        # type: (object) -> bool
        if isinstance(other, self.__class__) and self.data == other.data:
            return True
        else:
            return False

    def __ne__(self, other):
        # type: (object) -> bool
        if self == other:
            return False
        else:
            return True


class JSONObject(object):
    """
    This is a base class for building JSON objects to be used in Gladson response objects. Casting a `JSONObject` as a
    string will produce a JSON-compatible string in a predictable format.
    """

    keys_properties = OrderedDict()  # type: Dict

    def __init__(
        self,
        data  # type: Union[str, bytes, Dict]
    ):
        # type: () -> None
        """
        In child classes, initialization should accept arguments matching the JSON object's properties, and should
        assign these explicitly to attributes of the same name.

        :param data:

            A JSON representation of an object, or a python dictionary resulting from parsing
            a JSON representation of an object using the built-in `json` module.
        """
        self._string = None
        self.data = data

    @property
    def data(self):
        # type: () -> Dict
        """
        An ordered dictionary of the data represented by this object, in formats suitable for
        JSON serialization.
        """
        d = OrderedDict()
        for k, v in self.items():
            if v is None:
                continue
            if isinstance(v, (JSONArray, JSONObject)):
                v = v.data
            elif isinstance(v, collections.Sequence) and not isinstance(v, (str, bytes)):
                v = JSONArray(v).data
            elif isinstance(v, date):
                v = v.strftime('%m/%d/%Y')
            elif isinstance(v, datetime):
                v = v.strftime('%Y-%m-%dT%H:%M:%S%z')
            elif isinstance(v, Decimal):
                iv = int(v)
                if iv == v:
                    v = iv
                else:
                    v = float(v)
            d[k] = v
        return d

    @data.setter
    def data(
        self,
        data  # type: Union[str, bytes, Dict]
    ):
        if isinstance(data, bytes):
            data = str(data, 'utf-8')  # type: str
        if isinstance(data, str):
            self._string = data
            data = loads(data, object_hook=OrderedDict)  # type: Dict
        for k, v in data.items():
            if (v is None) or v == '':
                continue
            if k not in self.keys_properties:
                raise GladsonReplicationAPIKeyWarning(k)
            try:
                a = self.keys_properties[k]  # type: str
            except KeyError as e:
                raise GladsonReplicationAPIKeyWarning(
                    'An unrecognized attribute ("%s") was encountered while parsing:\n\n%s' % (
                        k,
                        str(data)
                    )
                )
            setattr(self, a, v)

    def items(self):
        # type: () -> Iterable[Tuple[str]]
        """
        Iterates through the key/value pairs of the corresponding JSON object.

        :return:

            An iterable of 2-item tuples representing JSON key/value pairs.
        """
        for k, a in self.keys_properties.items():
            v = getattr(self, a)
            if v is not None:
                yield k, v

    def keys(self):
        """
        :return: The keys from the JSON object represented.
        """
        # type: () -> Iterable[str]
        for k, v in self.items():
            yield k

    def values(self):
        # type: () -> Iterable[str]
        """
        :return: The values from the JSON object represented.
        """
        for k, v in self.items():
            yield v

    def __str__(self):
        # type: () -> str
        """
        :return: A JSON representation of this object.
        """
        d = OrderedDict()
        for k, v in self.data.items():
            d[k] = v
        return dumps(d)

    def __repr__(self):
        # type: () -> str
        """
        :return: A text representation of this object suitable for evaluating in Python.
        """
        items = tuple(self.items())
        return '%s(%s)' % (
            self.__class__.__name__.split('.')[-1],
            (
                '\n    ' + ',\n    '.join(
                    (
                        self.keys_properties[k] + '=' + re.sub(
                            r'(\r?\n)\s*',
                            r'',
                            re.sub(
                                r'(,\r?\n)\s*',
                                r', ',
                                repr(v)
                            )
                        )
                    )
                    for k, v in items
                    if v is not None
                ) + '\n'
                if items
                else ''
            )
        )

    def __eq__(self, other):
        # type: (object) -> bool
        if isinstance(other, self.__class__) and self.data == other.data:
            return True
        else:
            return False

    def __ne__(self, other):
        # type: (object) -> bool
        if self == other:
            return False
        else:
            return True


if Mapping is not None:
    class JSONObjectType(Mapping, JSONObject):
        pass


class LoginResponse(JSONObject):

    keys_properties = OrderedDict([
        ('AccessToken', 'access_token'),
        ('ExpiresAtUnixEpoch', 'expires_at_unix_epoch')
    ])

    def __init__(
        self,
        data=None,  # type: Optional[Union[str, bytes, Dict]]
        access_token=None,  # type: Optional[str]
        expires_at_unix_epoch=None,  # type: Optional[int]
    ):
        """
        :param access_token:

            A 28 character access token.

        :param expires_at_unix_epoch:

            An integer representing the time since the epoch (1970) at which the access token will expire.
        """
        self.access_token = access_token  # Optional[str]
        self.expires_at_unix_epoch = expires_at_unix_epoch  # Optional[int]
        super().__init__(data)
        #if data is not None:
        #    self.data = data


class LogoutResponse(JSONObject):

    keys_properties = OrderedDict([
        ('Username', 'user_name'),
        ('Token', 'token')
    ])

    def __init__(
        self,
        data=None,  # type: Optional[Union[str, bytes, Dict]]
        user_name=None,  # type: Optional[str]
        token=None,  # type: Optional[str]
    ):
        """
        :param user_name:

            The account user-name.

        :param token:

            The system-provided token for the authorization session.
        """
        self.user_name = user_name  # Optional[str]
        self.token = token  # Optional[str]
        super().__init__(data)
        #if data is not None:
        #    self.data = data


class Category(JSONObject):
    """
    Properties:

        - name (str)
        - code (str)
    """

    keys_properties = OrderedDict([
        ('Name', 'name'),
        ('Code', 'code')
    ])

    def __init__(
        self,
        data=None,  # type: Optional[Union[str, bytes, Dict]]
        name=None,  # type: Optional[str]
        code=None  # type: Optional[str]
    ):
        # type: (...) -> None
        """

        :param name:

            Gladson classification of products based on similar attributes

        :param code:

            8-character or less code for category name
        """
        self.name = name  # type: Optional[str]
        self.code = code  # type: Optional[str]
        super().__init__(data)
        #if data is not None:
        #    self.data = data


class Categories(JSONObject):
    """
    Properties:

        - category (Sequence[Category])
    """

    keys_properties = OrderedDict([
        ('Category', 'category')
    ])

    def __init__(
        self,
        data=None,  # type: Optional[Union[str, bytes, Dict]]
        category=None  # type: Sequence[Category]
    ):
        # type: () -> None
        self.category = category  # type: Sequence[Category]
        super().__init__(data)
        #if data is not None:
        #    self.data = data

    @property
    def data(self):
        return super().data

    @data.setter
    def data(
        self,
        data  # type: Union[str, bytes, Dict]
    ):
        if isinstance(data, bytes):
            data = str(data, 'utf-8')  # type: Union[str, bytes, Dict]
        if isinstance(data, str):
            data = loads(data, object_hook=OrderedDict)  # type: Union[str, bytes, Dict]
        for k, v in data.items():
            if v is None:
                continue
            try:
                a = self.keys_properties[k]  # type: str
            except KeyError as e:
                raise GladsonReplicationAPIKeyWarning(
                    'An unrecognized attribute ("%s") was encountered while parsing:\n\n%s' % (
                        k,
                        str(data)
                    )
                )
            if k == 'Category':
                if isinstance(v, Dict):
                    v = JSONArray([
                        Category(v)
                    ])
                else:
                    v = JSONArray([
                        Category(c) for c in v if c
                    ])
            setattr(self, a, v)


class Asset(JSONObject):
    """
    Properties:
        - asset_type (str):
          + "image"
          + "video"
          + "document"
          + "other"
        - asset_sub_type (str): A designation further describing the content and/or it's intended application. For
          example: "Ecom" or "Pog".
        - file_format (str): The asset's file format/extension. For example: "Jpeg" or "Png".
        - max_quality (str): A string or integer describing the highest quality in which the asset is available. For
          images, this is an integer representing the number if pixels along the longest side of the image. For example:
          "1000" or "500".
        - is_base (bool): If `True`—this asset is delivered as a base64-encoded character string rather than raw bytes.
    """

    keys_properties = OrderedDict([
        ('AssetType', 'asset_type'),
        ('AssetSubType', 'asset_sub_type'),
        ('FileFormat', 'file_format'),
        ('MaxQuality', 'max_quality'),
        ('IsBase":', 'is_base')
    ])

    def __init__(
        self,
        data=None,  # type: Optional[Union[str, bytes, Dict]]
        asset_type=None,  # type: Optional[str]
        asset_sub_type=None,  # type: Optional[str]
        file_format=None,  # type: Optional[str]
        max_quality=None,  # type: Optional[str]
        is_base=None  # type: Optional[bool]
    ):
        # type: (...) -> None
        self.asset_type = asset_type  # type: Optional[str]
        self.asset_sub_type = asset_sub_type  # type: Optional[str]
        self.file_format = file_format  # type: Optional[str]
        self.max_quality = max_quality  # type: Optional[str]
        self.is_base = is_base  # type: Optional[bool]
        super().__init__(data)
        #if data is not None:
        #    self.data = data

    @property
    def data(self):
        return super().data

    @data.setter
    def data(
        self,
        data  # type: Union[str, bytes, Dict]
    ):
        if isinstance(data, bytes):
            data = str(data, 'utf-8')  # type: Union[str, bytes, Dict]
        if isinstance(data, str):
            data = loads(data, object_hook=OrderedDict)  # type: Union[str, bytes, Dict]
        for k, v in data.items():
            if v is None:
                continue
            try:
                a = self.keys_properties[k]  # type: str
            except KeyError as e:
                raise GladsonReplicationAPIKeyWarning(
                    'An unrecognized attribute ("%s") was encountered while parsing:\n\n%s' % (
                        k,
                        str(data)
                    )
                )
            if k == 'IsBase' and isinstance(v, str):
                v = True if v.strip().lower() == 'true' else False
            setattr(self, a, v)


class ProductSearchResult(JSONObject):
    """
    Properties:

        - date_and_time_of_reception (date): The date that the product was received and processed by Gladson.
        - upc (str): The product’s "Universal Product Code", in GTIN-14 format.
        - modifier (str): A single-digit string of text distinguishing variations of an item which share the same UPC.
        - brand (str): The brand name of the product.
        - categories (Sequence[Category]): A sequence of "categories"—names for a set of products having similar
          attribution and taxonomy.
        - language (str): The language in which properties of this product are expressed. For example: "English".
        - description (str): A generic description of the product, followed by distinguishing variables. For example:
          "Beef Pattie Fritters, Country Fried Steak", "Ink Cartridge, Standard-Capacity, Cyan/Magenta/Yellow, T200520",
          or "Mineral Spirits, Odorless".
        - assets (Sequence[Asset]): A sequence of `Asset` instances with information concerning media assets which
          are available for this product.
    """
    keys_properties = OrderedDict([
        ('DateAndTimeOfReception', 'date_and_time_of_reception'),
        ('Upc', 'upc'),
        ('Modifier', 'modifier'),
        ('Brand', 'brand'),
        ('Categories', 'categories'),
        ('Language', 'language'),
        ('Description', 'description'),
        ('Assets', 'assets')
    ])

    def __init__(
        self,
        data=None,  # type: Optional[Union[str, bytes, Dict]]
        date_and_time_of_reception=None,  # type: Optional[date]
        upc=None,  # type: Optional[str]
        modifier=None,  # type: Optional[str]
        brand=None,  # type: Optional[str]
        source=None,  # type: Optional[str]
        categories=None,  # type: Optional[Sequence[Category]]
        language=None,  # type: Optional[str]
        description=None,  # type: Optional[str]
        assets=None  # type: Optional[Sequence[Asset]]
    ):
        # type: (...) -> None
        self.date_and_time_of_reception = date_and_time_of_reception  # type: Optional[Sequence[date]]
        self.upc = upc  # type: Optional[str]
        self.modifier = modifier  # type: Optional[str]
        self.brand = brand  # type: Optional[str]
        self.source = source  # type: Optional[str]
        self.categories = categories  # type: Optional[Sequence[Category]]
        self.language = language  # type: Optional[str]
        self.description = description  # type: Optional[str]
        self.assets = assets  # type: Optional[Sequence[Asset]]
        super().__init__(data)
        #if data is not None:
        #    self.data = data

    @property
    def data(self):
        return super().data

    @data.setter
    def data(
        self,
        data  # type: Union[str, bytes, Dict]
    ):
        if isinstance(data, bytes):
            data = str(data, 'utf-8')  # type: Union[str, bytes, Dict]
        if isinstance(data, str):
            data = loads(data, object_hook=OrderedDict)  # type: Union[str, bytes, Dict]
        for k, v in data.items():
            if v is None:
                continue
            try:
                a = self.keys_properties[k]  # type: str
            except KeyError as e:
                raise GladsonReplicationAPIKeyWarning(
                    'An unrecognized attribute ("%s") was encountered while parsing:\n\n%s' % (
                        k,
                        str(data)
                    )
                )
            if k == 'DateAndTimeOfReception':
                v = str2date(v)
            elif k == 'Categories':
                v = Categories(v)
            elif k == 'Assets':
                if hasattr(v, 'keys'):
                    v = JSONArray([
                        Asset(v)
                    ])
                else:
                    v = JSONArray([
                        Asset(asset) for asset in v
                    ])
            setattr(self, a, v)


class AdditionalIngredients(JSONObject):
    """
    Properties:
        - additional_ingredient (str): A list of "additional" ingredients.
        - additional_ingredients_value (str)
    """

    keys_properties = OrderedDict([
        ('AdditionalIngredient', 'additional_ingredient'),
        ('AdditionalIngredientsValue', 'additional_ingredients_value')
    ])

    def __init__(
        self,
        data=None,  # type: Optional[Union[str, bytes, Dict]]
        additional_ingredient=None,  # type: Optional[Sequence[str]]
        additional_ingredients_value=None  # type: Optional[str]
    ):
        # type: (...) -> None
        self.additional_ingredient = additional_ingredient  # type: Optional[Sequence[str]]
        self.additional_ingredients_value = additional_ingredients_value  # type: Optional[str]
        super().__init__(data)
        #if data is not None:
        #    self.data = data

    @property
    def data(self):
        return super().data

    @data.setter
    def data(
        self,
        data  # type: Union[str, bytes, Dict]
    ):
        if isinstance(data, bytes):
            data = str(data, 'utf-8')  # type: Union[str, bytes, Dict]
        if isinstance(data, str):
            data = loads(data, object_hook=OrderedDict)  # type: Union[str, bytes, Dict]
        for k, v in data.items():
            if v is None:
                continue
            try:
                a = self.keys_properties[k]  # type: str
            except KeyError as e:
                raise GladsonReplicationAPIKeyWarning(
                    'An unrecognized attribute ("%s") was encountered while parsing:\n\n%s' % (
                        k,
                        str(data)
                    )
                )
            if k == 'AdditionalIngredient':
                v = JSONArray([v]) if isinstance(v, str) else JSONArray(v)
            setattr(self, a, v)


class Ingredients(JSONObject):
    """
    Properties:

        - ingredient (Sequence[str]): A list of ingredients.
        - additional_ingredients (AdditionalIngredients): A container object for a list of "additional" ingredients.
    """

    keys_properties = OrderedDict([
        ('Ingredient', 'ingredient'),
        ('AdditionalIngredients', 'additional_ingredients')
    ])

    def __init__(
        self,
        data=None,  # type: Optional[Union[str, bytes, Dict]]
        ingredient=None,  # type: Optional[Sequence[str]]
        additional_ingredients=None  # type: Optional[AdditionalIngredients]
    ):
        # type: (...) -> None
        self.ingredient = ingredient  # type: Optional[Sequence[str]]
        self.additional_ingredients = additional_ingredients  # type: Optional[AdditionalIngredients]
        super().__init__(data)
        #if data is not None:
        #    self.data = data

    @property
    def data(self):
        return super().data

    @data.setter
    def data(
        self,
        data  # type: Union[str, bytes, Dict]
    ):
        if isinstance(data, bytes):
            data = str(data, 'utf-8')  # type: Union[str, bytes, Dict]
        if isinstance(data, str):
            data = loads(data, object_hook=OrderedDict)  # type: Union[str, bytes, Dict]
        for k, v in data.items():
            if v is None:
                continue
            try:
                a = self.keys_properties[k]  # type: str
            except KeyError as e:
                raise GladsonReplicationAPIKeyWarning(
                    'An unrecognized attribute ("%s") was encountered while parsing:\n\n%s' % (
                        k,
                        str(data)
                    )
                )
            if k == 'Ingredient':
                v = JSONArray([v]) if isinstance(v, str) else JSONArray(v)
            elif k == 'AdditionalIngredients':
                v = AdditionalIngredients(v)
            setattr(self, a, v)


class Nutrient(JSONObject):
    """
    Properties:

        - is_or_contains (bool): If `True`, this implies that the product either has or is the
          ingredient/nutrient/claim named. This is used with health claims (is) and allergens (contains)
        - name (str): The name of the nutrient, or if `is_or_contains` is `True`—the name of the ingredient or claim.
        - percentage (Decimal): The amount of this nutrient contained in a serving, expressed in terms of a percentage
          of the recommended daily consumption of this nutrient as dictated by the regionally relevant governing
          organization.
        - quantity (Decimal): The amount of this nutrient contained in a serving, expressed in terms of the measurement
          indicated by the `uom`.
        - uom (str): The measurement in which the `quantity` of this nutrient is expressed.
        - value_prepared_type (bool): Does this value refer to a preparation which differs nutritionally from the
          unprepared state of the product?
    """

    keys_properties = OrderedDict([
        ('IsOrContains', 'is_or_contains'),
        ('Name', 'name'),
        ('Percentage', 'percentage'),
        ('Quantity', 'quantity'),
        ('UOM', 'uom'),
        ('ValuePreparedType', 'value_prepared_type')
    ])

    def __init__(
        self,
        data=None,  # type: Optional[Union[str, bytes, Dict]]
        is_or_contains=None,  # type: Optional[bool]
        name=None,  # type: Optional[str]
        percentage=None,  # type: Optional[Decimal]
        quantity=None,  # type: Optional[Decimal]
        uom=None,  # type: Optional[str]
        value_prepared_type=None  # type: Optional[bool]
    ):
        # type: (...) -> None
        self.is_or_contains = is_or_contains  # type: Optional[bool]
        self.name = name  # type: Optional[str]
        self.percentage = percentage  # type: Optional[Decimal]
        self.quantity = quantity  # type: Optional[Decimal]
        self.uom = uom  # type: Optional[str]
        self.value_prepared_type = value_prepared_type  # type: Optional[bool]
        super().__init__(data)
        #if data is not None:
        #    self.data = data

    @property
    def data(self):
        return super().data

    @data.setter
    def data(
        self,
        data  # type: Union[str, bytes, Dict]
    ):
        if isinstance(data, bytes):
            data = str(data, 'utf-8')  # type: Union[str, bytes, Dict]
        if isinstance(data, str):
            data = loads(data, object_hook=OrderedDict)  # type: Union[str, bytes, Dict]
        for k, v in data.items():
            if v is None:
                continue
            try:
                a = self.keys_properties[k]  # type: str
            except KeyError as e:
                raise GladsonReplicationAPIKeyWarning(
                    'An unrecognized attribute ("%s") was encountered while parsing:\n\n%s' % (
                        k,
                        str(data)
                    )
                )
            if k == 'IsOrContains' and isinstance(v, str):
                v = True if v.strip().lower() == 'true' else False
            elif k == 'ValuePreparedType' and isinstance(v, str):
                v = True if v.strip() == '1' else False
            elif k in ('Percentage', 'Quantity'):
                v = Decimal(v)
            setattr(self, a, v)


class Variant(JSONObject):
    """
    Properties:

        - serving_size_text (str): A quantitative measure of the serving size (expressed as text). A complete
          description of the serving size can be assembled by concatenating this with the *serving_size_uom*.
        - serving_size_uom (str): The unit of measure in which the `serving_size_text` is expressed.
        - servings_per_container: A measure of how many servings are contained in the package (can be approximated), as
          described on the package.
        - upc (str): The product's 14- or 15-digit universal product code, also known as a "GTIN".
        - nutrient (Sequence[Nutrient]): A sequence of objects representing information about individual nutrients.
        - value_prepared_type (bool): Do these values refer to a preparation which differs nutritionally from the
          unprepared state of the product?
        - serving_size_in_grams (str): The serving size, in grams, represented as text (typically including the
          measurement abbreviation "g" as well as the numeric measure).
        - variant_value (str)
        - serving_size_prepared (str)
    """

    keys_properties = OrderedDict([
        ('ServingSize-InGrams', 'serving_size_in_grams'),
        ('ServingSizeText', 'serving_size_text'),
        ('ServingSizeUOM', 'serving_size_uom'),
        ('ServingSize_Prepared', 'serving_size_prepared'),
        ('ServingsPerContainer', 'servings_per_container'),
        ('Upc', 'upc'),
        ('Nutrient', 'nutrient'),
        ('ValuePreparedType', 'value_prepared_type'),
        ('VariantValue', 'variant_value')
    ])

    def __init__(
        self,
        data=None,  # type: Optional[Union[str, bytes, Dict]]
        serving_size_text=None,  # type: Optional[str]
        serving_size_uom=None,  # type: Optional[str]
        servings_per_container=None,  # type: Optional[str]
        upc=None,  # type: Optional[str]
        nutrient=None,  # type: Optional[Sequence[Nutrient]]
        value_prepared_type=None,  # type: Optional[bool]
        serving_size_in_grams=None,  # type: Optional[str]
        variant_value=None,  # type: Optional[str]
        serving_size_prepared=None,  # type: Optional[str]
    ):
        # type: (...) -> None
        self.serving_size_text = serving_size_text  # type: Optional[str]
        self.serving_size_uom = serving_size_uom  # type: Optional[str]
        self.servings_per_container = servings_per_container  # type: Optional[str]
        self.upc = upc  # type: Optional[str]
        self.nutrient = nutrient  # type: Optional[Sequence[Nutrient]]
        self.value_prepared_type = value_prepared_type  # type: Optional[bool]
        self.serving_size_in_grams = serving_size_in_grams  # type: Optional[str]
        self.variant_value = variant_value  # type: Optional[str]
        self.serving_size_prepared = serving_size_prepared  # type: Optional[str]
        super().__init__(data)
        #if data is not None:
        #    self.data = data

    @property
    def data(self):
        return super().data

    @data.setter
    def data(
        self,
        data  # type: Union[str, bytes, Dict]
    ):
        if isinstance(data, bytes):
            data = str(data, 'utf-8')  # type: Union[str, bytes, Dict]
        if isinstance(data, str):
            data = loads(data, object_hook=OrderedDict)  # type: Union[str, bytes, Dict]
        for k, v in data.items():
            if v is None:
                continue
            try:
                a = self.keys_properties[k]  # type: str
            except KeyError as e:
                raise GladsonReplicationAPIKeyWarning(
                    'An unrecognized attribute ("%s") was encountered while parsing:\n\n%s' % (
                        k,
                        str(data)
                    )
                )
            if k == 'Nutrient':
                if hasattr(v, 'keys'):
                    v = JSONArray([
                        Nutrient(v)
                    ])
                else:
                    v = JSONArray([
                        Nutrient(n) for n in v
                    ])
            elif k == 'ValuePreparedType' and isinstance(v, str):
                v = True if v.strip() == '1' else False
            setattr(self, a, v)


class NutritionFacts(JSONObject):
    """
    Properties

        - variant (Sequence[Variant]): A sequence of objects representing nutrition facts for each
          variant of an item.
    """

    keys_properties = OrderedDict([
        ('Variant', 'variant')
    ])

    def __init__(
        self,
        data=None,  # type: Optional[Union[str, bytes, Dict]]
        variant=None  # type: Optional[Sequence[Variant]]
    ):
        # type: (...) -> None
        self.variant = variant  # type: Optional[Sequence[Variant]]
        super().__init__(data)
        #if data is not None:
        #    self.data = data

    @property
    def data(self):
        return super().data

    @data.setter
    def data(
        self,
        data  # type: Union[str, bytes, Dict]
    ):
        if isinstance(data, bytes):
            data = str(data, 'utf-8')  # type: Union[str, bytes, Dict]
        if isinstance(data, str):
            data = loads(data, object_hook=OrderedDict)  # type: Union[str, bytes, Dict]
        for k, v in data.items():
            if v is None:
                continue
            try:
                a = self.keys_properties[k]  # type: str
            except KeyError as e:
                raise GladsonReplicationAPIKeyWarning(
                    'An unrecognized attribute ("%s") was encountered while parsing:\n\n%s' % (
                        k,
                        str(data)
                    )
                )
            if k == 'Variant':
                if hasattr(v, 'keys'):
                    v = JSONArray([
                        Variant(v)
                    ])
                else:
                    v = JSONArray([
                        Variant(vnf) for vnf in v
                    ])
            setattr(self, a, v)


class ProductDetails(JSONObject):
    """
    Properties:

        Identification:

        - upc (str): A 14- or 15-digit universal product code, also known as a GTIN.
        - legacy_upc (str): A depracated, 12-digit representation of the product's universal product code.
        - modifier (str): A single-character string distinguishing variations of an item which share the same UPC.

        Classification:

        - segment (str): A high-level product classification.
            + "FOOD": Food and Beverages
            + "HBC": Health, Beauty & Cosmetics
            + "GM": General Merchandise
        - categories (Categories): A container object for a sequence of `Category` instances denoting classification
          of the product within Gladson's taxonomy.

        General Information:

        - source_label (str): The source(s) from whom the product information was received, for example:
          "Gladson" or "Manufacturer".
        - language (str): The language in which properties of this product are expressed. For example: "English".
        - description (str): The `item_name` plus any product "attributes", separated by commas—for example:
          "Toothpaste, Fluoride, Clean Mint" or "Pasta Sauce, Garden Veggie".
        - item_name (str): The simplest, most basic description of the product—for example: "Coffee", "Cookies", or
          "Tea").
        - directions (str):  Any instructions for use, as listed on the product.
        - brand (str): The most generalized name of the brand which can be found on the product/packaging.
        - product_line (str): A branding subset, also commonly referred to as a sub-brand. For example: in "Ford Focus",
          "Ford" would be the `brand` and "Focus" the `product_line`.
        - variant (str): A variable attribute of the product such as a flavor, fragrance, taste, or color.
        - warnings (str): Cautionary statements and warning as shown on the product's packaging.
        - is_discontinued (bool)
        - assets (Sequence[Asset]): A sequence of `Asset` instances containing information about a media asset, and
          which can be used to retrieve the associated file.

        Company Information:

        - manufacturer (str): The name of the company which manufactured this product.
        - phone (str): The manufacturer phone number, as displayed on the product/packaging.
        - address (str): The address displayed on the box for the brand/manufacturer/distributor of the product.
        - copyright (str): Copyright information from the manufacturer as shown on the package.

        Dates:

        - date_updated (date): The date on which this product information was captured/posted/updated. This is the only
          date-property defined in the documentation most recently provided by Gladson at the time of writing, however
          it is sparsely populated.
        - post_date (date): (inferred) The date on which this product information was captured/posted/updated.
        - postdate (date): (inferred) The date on which this product information was captured/posted/updated.
        - date_and_time_of_reception (date): (inferred) The date on which this product information was received/updated.
          Note: This date is more frequently present for a product than the previous 3, however is often significantly
          later than the `date_updated`, `post_date`, or `postdate`.

        Drug Information:

        - drug_interactions (str)
        - indications (str): Indicates what ailments a product (typically an over-the-counter medication) can/should
          be used to treat.

        Food and Beverage Information:

        - has_nutrition (bool): Indicates whether nutrition information is available for this product.
        - ingredients (Ingredients): A container object for a sequence of ingredients contained in the product.
        - kosher1 (int): When present, this indicates that a symbol known to indicate a Kosher certification was found
          on the product's label. The number indicated correlates to a specific symbol/certification.
        - kosher2 (int): See `kosher1`.
        - kosher3 (int): See `kosher1`.
        - kosher4 (int): See `kosher1`.
        - kosher5 (int): See `kosher1`.
        - nutrition_facts (NutritionFacts): A container object holding one or more `Variant` instances, each of which
          contains a set of nutrition facts which vary depending on preparation state and/or serving size.
        - value_prepared_count (int): The number of different nutrition fact variants provided which represent a
          *prepared* variation of this product.

        Size:

        - item_size (Decimal): The number of units in the product expressed in terms of the `item_measure` or `uom`.
        - item_measure (str): The unit-of-measure abbreviation (expressed in lower-case characters, without periods),
          as relates to the numeric quantity indicated by the `item_size` property.
            + "ea": Abbreviated from "each", this indicates that `item_size` is a unit quantity.
            + "g": Gram
            + "cg": Centigram
            + "dg": Decigram
            + "hg": Hectogram
            + "kg": Kilogram
            + "lb": Pound
            + "mcg": Microgram
            + "mg": Milligram
            + "mt": Metric Ton
            + "oz": Ounce
            + "t": Ton
            + "cm": Centimeter
            + "dm": Decimeter
            + "ft": Foot
            + "in": Inch
            + "km": Kilometer
            + "m": Meter
            + "mm": Millimeter
            + "sf": Square Foot
            + "sm": Square Meter
            + "sy": Square Yard
            + "yd": Yard
        - uom (str): This is the same as the `item_measure` property, but is expressed in upper-case characters, and
          is set to `None` rather than "ea" if `item_size` is a unit quantity.
        - extended_size (str):  A text description of the product's size, as shown on the package. For example:
          "3 desserts [9 oz (270 ml)]", "22 oz (1 lb 6 oz) 624 g", or "16.9 fl oz (500 ml)".

        Measurements:

        - width (Decimal): The width of the item (in-package), measured in inches.
        - height (Decimal): The height of the item (in-package), measured in inches.
        - depth (Decimal): The length of of the item (in-package), from front to back, measured in inches.
        - product_weight (Decimal): The weight of the product (in-package), measured in ounces.

        Etcetera:

        - product_details (str): Additional product information from the package which was not collected in any other
          fields.
    """

    keys_properties = OrderedDict([
        ('Upc', 'upc'),
        ('Brand', 'brand'),
        ('Sourcelabel', 'source_label'),
        ('Language', 'language'),
        ('Postdate', 'postdate'),
        ('Post Date', 'post_date'),
        ('Date', 'date_updated'),
        ('DateAndTimeOfReception', 'date_and_time_of_reception'),
        ('Description', 'description'),
        ('Categories', 'categories'),
        ('Manufacturer', 'manufacturer'),
        ('Modifier', 'modifier'),
        ('Assets', 'assets'),
        ('Address', 'address'),
        ('Copyright', 'copyright'),
        ('Depth', 'depth'),
        ('Directions', 'directions'),
        ('Extendedsize', 'extended_size'),
        ('Hasnutrition', 'has_nutrition'),
        ('Height', 'height'),
        ('Indications', 'indications'),
        ('Ingredients', 'ingredients'),
        ('Itemmeasure', 'item_measure'),
        ('Itemname', 'item_name'),
        ('Itemsize', 'item_size'),
        ('Kosher1', 'kosher1'),
        ('Kosher2', 'kosher2'),
        ('Kosher3', 'kosher3'),
        ('Kosher4', 'kosher4'),
        ('Kosher5', 'kosher5'),
        ('Legacy UPC', 'legacy_upc'),
        ('Modifier', 'modifier'),
        ('Nutritionfacts', 'nutrition_facts'),
        ('Phone', 'phone'),
        ('Productdetails', 'product_details'),
        ('Productline', 'product_line'),
        ('Productweight', 'product_weight'),
        ('Uom', 'uom'),
        ('Valuepreparedcount', 'value_prepared_count'),
        ('Variant', 'variant'),
        ('Warnings', 'warnings'),
        ('Width', 'width'),
        ('Ingredients', 'ingredients'),
        ('NutritionFacts', 'nutrition_facts'),
        ('Segment', 'segment'),
        ('IsDiscontinued', 'is_discontinued'),
        ('Druginteractions', 'drug_interactions')
    ])

    def __init__(
        self,
        data=None,  # type: Optional[Union[str, bytes, Dict]]
        brand=None,  # type: Optional[str]
        source_label=None,  # type: Optional[str]
        language=None,  # type: Optional[str]
        post_date=None,  # type: Optional[date]
        postdate=None,  # type: Optional[date]
        date_updated=None,  # type: Optional[date]
        date_and_time_of_reception=None,  # type: Optional[date]
        categories=None,  # type: Optional[Categories]
        manufacturer=None,  # type: Optional[str]
        modifier=None,  # type: Optional[str]
        assets=None,  # type: Optional[Sequence[Asset]]
        address=None,  # type: Optional[str]
        copyright=None,  # type: Optional[str]
        depth=None,  # type: Optional[Decimal]
        description=None,  # type: Optional[str]
        directions=None,  # type: Optional[str]
        extended_size=None,  # type: Optional[str]
        has_nutrition=None,  # type: Optional[bool]
        height=None,  # type: Optional[Decimal]
        indications=None,  # type: Optional[str]
        ingredients=None,  # type: Optional[Ingredients]
        item_measure=None,  # type: Optional[str]
        item_name=None,  # type: Optional[str]
        item_size=None,  # type: Optional[str]
        kosher1=None,  # type: Optional[int]
        kosher2=None,  # type: Optional[int]
        kosher3=None,  # type: Optional[int]
        kosher4=None,  # type: Optional[int]
        kosher5=None,  # type: Optional[int]
        legacy_upc=None,  # type: Optional[str]
        nutrition_facts=None,  # type: Optional[NutritionFacts]
        phone=None,  # type: Optional[str]
        product_details=None,  # type: Optional[str]
        product_line=None,  # type: Optional[str]
        product_weight=None,  # type: Optional[Decimal]
        uom=None,  # type: Optional[str]
        upc=None,  # type: Optional[str]
        value_prepared_count=None,  # type: Optional[int]
        variant=None,  # type: Optional[str]
        warnings=None,  # type: Optional[str]
        width=None,  # type: Optional[Decimal]
        segment=None,  # type: Optional[str]
        is_discontinued=None,  # type: Optional[bool]
        drug_interactions=None,  # type: Optional[str]
    ):
        # type: (...) -> None
        self.brand = brand  # type: Optional[str]
        self.source_label = source_label  # type: Optional[str]
        self.language = language  # type: Optional[str]
        self.post_date = post_date  # type: Optional[date]
        self.postdate = postdate  # type: Optional[date]
        self.date_updated = date_updated  # type: Optional[date]
        self.date_and_time_of_reception = date_and_time_of_reception  # type: Optional[date]
        self.categories = categories  # type: Optional[Categories]
        self.manufacturer = manufacturer  # type: Optional[str]
        self.modifier = modifier  # type: Optional[str]
        self.assets = assets  # type: Optional[Sequence[Asset]]
        self.address = address  # type: Optional[str]
        self.copyright = copyright  # type: Optional[str]
        self.depth = depth  # type: Optional[Decimal]
        self.description = description  # type: Optional[str]
        self.directions = directions  # type: Optional[str]
        self.extended_size = extended_size  # type: Optional[str]
        self.has_nutrition = has_nutrition  # type: Optional[bool]
        self.height = height  # type: Optional[Decimal]
        self.indications = indications  # type: Optional[str]
        self.ingredients = ingredients  # type: Optional[Ingredients]
        self.item_measure = item_measure  # type: Optional[str]
        self.item_name = item_name  # type: Optional[str]
        self.item_size = item_size  # type: Optional[str]
        self.kosher1 = kosher1  # type: Optional[int]
        self.kosher2 = kosher2  # type: Optional[int]
        self.kosher3 = kosher3  # type: Optional[int]
        self.kosher4 = kosher4  # type: Optional[int]
        self.kosher5 = kosher5  # type: Optional[int]
        self.legacy_upc = legacy_upc  # type: Optional[str]
        self.nutrition_facts = nutrition_facts  # type: Optional[NutritionFacts]
        self.phone = phone  # type: Optional[str]
        self.product_details = product_details  # type: Optional[str]
        self.product_line = product_line  # type: Optional[str]
        self.product_weight = product_weight  # type: Optional[Decimal]
        self.uom = uom  # type: Optional[str]
        self.upc = upc  # type: Optional[str]
        self.value_prepared_count = value_prepared_count  # type: Optional[int]
        self.variant = variant  # type: Optional[str]
        self.warnings = warnings  # type: Optional[str]
        self.width = width  # type: Optional[Decimal]
        self.segment = segment  # type: Optinal[str]
        self.is_discontinued = is_discontinued  # type: Optinal[bool]
        self.drug_interactions = drug_interactions  # type: Optinal[str]
        super().__init__(data)
        # if data is not None:
        #     self.data = data

    @property
    def data(self):
        return super().data

    @data.setter
    def data(
        self,
        data  # type: Union[str, bytes, Dict]
    ):
        if isinstance(data, bytes):
            data = str(data, 'utf-8')  # type: Union[str, bytes, Dict]
        if isinstance(data, str):
            data = loads(data, object_hook=OrderedDict)  # type: Union[str, bytes, Dict]
        for k, v in data.items():
            if v is None:
                continue
            try:
                a = self.keys_properties[k]  # type: str
            except KeyError as e:
                raise GladsonReplicationAPIKeyWarning(
                    'An unrecognized attribute ("%s") was encountered while parsing `%s`:\n\n%s' % (
                        k,
                        self.__class__.__name__,
                        str(data)
                    )
                )
            if k in ('Post Date', 'Date', 'DateAndTimeOfReception', 'Postdate'):
                if isinstance(v, str):
                    v = str2date(v)
            elif k == 'Categories':
                v = Categories(v)
            elif k == 'Assets':
                v = JSONArray([
                    Asset(asset) for asset in v
                ])
            elif k == 'NutritionFacts':
                v = NutritionFacts(v)
            elif k == 'Ingredients':
                v = Ingredients(v)
            elif k == 'ItemSize':
                v = Decimal(v)
            elif k in (
                'Depth', 'Height', 'Productweight', 'Width'
            ):
                v = Decimal(v)
            elif k in (
                'Kosher1', 'Kosher2', 'Kosher3', 'Kosher4', 'Kosher5',
                'Valuepreparedcount'
            ):
                v = int(v)
            elif k in ('Hasnutrition', 'IsDiscontinued') and isinstance(v, str):
                v = True if v.strip().lower() == 'true' else False
            setattr(self, a, v)
