import base64
import os
from datetime import date, timedelta

from decimal import Decimal
from random import randint
from time import sleep

from PIL import Image
from io import BytesIO

from gladson.replication import ProductDetails
from gladson.replication import ProductSearchResult
from gladson.replication import Replication
from gladson.replication.errors import ProductNotFound, NotAuthorized
from gladson.replication.response import str2date, Categories, Category, JSONArray, Ingredients, AdditionalIngredients, \
    NutritionFacts, Variant, Nutrient, Asset, JSONObject

NoneType = type(None)


def test_str2date(replication):
    # type: (Replication) -> None
    assert str2date('11/03/2016') == date(2016, 11, 3)
    assert str2date('2016/11/03') == date(2016, 11, 3)


def test_product(replication):
    # type: (Replication) -> None
    """
    This test randomly samples (at least) 10 dates and validates (at least) 10 products. The test concludes when
    both 10 different update-dates are included in the sample (minimum), and at least 10 products are represented in
    the sample.
    """
    sample_size = 10
    tested_products = 0
    tested_images = 0
    days = set()
    start = date(1999, 11, 1)
    span = (date.today() - start).days - 1
    while tested_products < sample_size or len(days) < sample_size or tested_images < sample_size:
        day = randint(0, span)
        if day in days:
            continue
        sample_date = start + timedelta(days=day)
        try:
            results = tuple(replication.product.search(
                start_date=sample_date,
                end_date=sample_date,
                echo=True
            ))
            # print('Search Results:\n%s' % str(results))
        except ProductNotFound as e:
            print('No products found on %s' % str(sample_date))
            continue
        for result in results: # type: ProductSearchResult
            print()
            print('Testing %s' % (result.upc + (result.modifier or '')))
            details = replication.product.details(  # type: ProductDetails
                result.upc,
                modifier=result.modifier,
                echo=True
            )
            print()
            print(repr(details))
            print()
            print(str(details))
            assert isinstance(details.brand, str)
            assert isinstance(details.source_label, (str, NoneType))
            assert isinstance(details.language, (str, NoneType))
            assert isinstance(details.postdate, date)
            assert isinstance(details.post_date, date)
            assert isinstance(details.date_updated, date)
            assert isinstance(details.date_and_time_of_reception, date)
            assert isinstance(details.description, str)
            assert isinstance(details.categories, Categories)
            for category in details.categories.category:
                assert isinstance(category, Category)
                assert isinstance(category.name, str)
                assert isinstance(category.code, str)
            assert isinstance(details.manufacturer, (str, NoneType))
            assert isinstance(details.modifier, (str, NoneType))
            assert isinstance(details.assets, (JSONArray, NoneType))
            if details.assets is not None:
                for asset in details.assets:
                    assert isinstance(asset, Asset)
                    assert isinstance(asset.asset_type, str)
                    assert isinstance(asset.asset_sub_type, str)
                    assert isinstance(asset.file_format, str)
                    assert isinstance(asset.max_quality, str)
                    assert isinstance(asset.is_base, (bool, NoneType))
            assert isinstance(details.address, (str, NoneType))
            assert isinstance(details.copyright, (str, NoneType))
            assert isinstance(details.depth, Decimal)
            assert isinstance(details.width, Decimal)
            assert isinstance(details.height, Decimal)
            assert isinstance(details.directions, (str, NoneType))
            assert isinstance(details.extended_size, str)
            assert isinstance(details.has_nutrition, bool)
            assert isinstance(details.indications, (str, NoneType))
            assert isinstance(details.ingredients, (Ingredients, NoneType))
            if details.ingredients is not None:
                for ingredient in details.ingredients.ingredient:
                    assert isinstance(ingredient, str)
                assert isinstance(details.ingredients.additional_ingredients, (AdditionalIngredients, NoneType))
                if details.ingredients.additional_ingredients is not None:
                    assert isinstance(
                        details.ingredients.additional_ingredients.additional_ingredients_value,
                        str
                    )
                    for additional_ingredient in details.ingredients.additional_ingredients.additional_ingredient:
                        assert isinstance(additional_ingredient, str)
            assert isinstance(details.item_measure, (str, NoneType))
            assert isinstance(details.item_name, (str, NoneType))
            assert isinstance(details.item_size, (str, NoneType))
            assert isinstance(details.kosher1, (int, NoneType))
            assert isinstance(details.kosher2, (int, NoneType))
            assert isinstance(details.kosher3, (int, NoneType))
            assert isinstance(details.kosher4, (int, NoneType))
            assert isinstance(details.legacy_upc, (str, NoneType))
            assert isinstance(details.nutrition_facts, (NutritionFacts, NoneType))
            if details.nutrition_facts is not None:
                assert isinstance(details.nutrition_facts.variant, JSONArray)
                for variant in details.nutrition_facts.variant:
                    assert isinstance(variant, Variant)
                    assert isinstance(variant.serving_size_text, (str, NoneType))
                    assert isinstance(variant.serving_size_uom, (str, NoneType))
                    assert isinstance(variant.servings_per_container, (str, NoneType))
                    assert isinstance(variant.upc, str)
                    assert isinstance(variant.value_prepared_type, (bool, NoneType))
                    assert isinstance(variant.nutrient, JSONArray)
                    for nutrient in variant.nutrient:
                        assert isinstance(nutrient, Nutrient)
                        assert isinstance(nutrient.is_or_contains, (bool, NoneType))
                        assert isinstance(nutrient.name, str)
                        assert isinstance(nutrient.percentage, (Decimal, NoneType))
                        assert isinstance(nutrient.quantity, (Decimal, NoneType))
                        assert isinstance(nutrient.uom, (str, NoneType))
                    assert isinstance(variant.variant_value, (str, NoneType))
                    assert isinstance(variant.serving_size_in_grams, (str, NoneType))
            assert isinstance(details.phone, (str, NoneType))
            assert isinstance(details.product_details, (str, NoneType))
            assert isinstance(details.product_line, (str, NoneType))
            assert isinstance(details.product_weight, (Decimal, NoneType))
            assert isinstance(details.product_weight, (Decimal, NoneType))
            assert isinstance(details.uom, (str, NoneType))
            assert isinstance(details.upc, str)
            assert isinstance(details.value_prepared_count, (int, NoneType))
            assert isinstance(details.variant, (str, NoneType))
            assert isinstance(details.warnings, (str, NoneType))
            assert isinstance(details.segment, (str, NoneType))
            assert isinstance(details.is_discontinued, (bool, NoneType))
            assert isinstance(details.drug_interactions, (str, NoneType))
            if isinstance(details.assets, JSONArray) and len(details.assets):
                i = 0
                assets = []
                for a in details.assets:
                    error = None
                    for i in range(4):
                        error = None
                        try:
                            assets.append(replication.product.asset(
                                upc=result.upc,
                                modifier=result.modifier,
                                asset_type=a.asset_type,
                                asset_sub_type=a.asset_sub_type,
                                file_format=a.file_format,
                                quality=a.max_quality,
                                echo=True
                            ))
                            break
                        except NotAuthorized as e:
                            break
                        except Exception as e:
                            error = e
                            sleep(i)
                    if error is not None:
                        raise error
                for asset in assets:
                    if asset is None:
                        continue
                    asset_bytes = asset.read()
                    if details.assets[i].is_base:
                        asset_bytes = base64.b64decode(asset_bytes)
                    image = Image.open(  # type: Image.Image
                        BytesIO(asset_bytes)
                    )
                    assert image.size[0] > 0
                    assert image.size[1] > 0
                    tested_images += 1
                    i += 1
            else:
                print('No images found for %s' % (result.upc + (result.modifier or '')))
            tested_products += 1
            break
        days.add(day)


if __name__ == '__main__':
    user, password = os.environ['GLADSON_LOGIN'].split(':')
    replication = Replication(
        user=user,
        password=password,
        user_token=os.environ['GLADSON_USER_TOKEN'],
        api_key=os.environ['GLADSON_API_KEY'],
        echo=True
    )
    test_product(replication)
