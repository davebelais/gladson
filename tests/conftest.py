import os

import pytest

from gladson.replication import Replication

GLADSON_LOGIN = None if 'GLADSON_LOGIN' not in os.environ else os.environ['GLADSON_LOGIN']
GLADSON_USER_TOKEN = None if 'GLADSON_USER_TOKEN' not in os.environ else os.environ['GLADSON_USER_TOKEN']
GLADSON_API_KEY = None if 'GLADSON_API_KEY' not in os.environ else os.environ['GLADSON_API_KEY']


def pytest_addoption(parser):
    parser.addoption(
        "--user",
        action="store",
        default=None if GLADSON_LOGIN is None else GLADSON_LOGIN.split(':')[0],
        help="user: Gladson API user name"
    )
    parser.addoption(
        "--password",
        action="store",
        default=None if GLADSON_LOGIN is None else GLADSON_LOGIN.split(':')[-1],
        help="password: Gladson API password"
    )
    parser.addoption(
        "--user_token",
        action="store",
        default=GLADSON_USER_TOKEN,
        help="user_token: Gladson user token"
    )
    parser.addoption(
        "--api_key",
        action="store",
        default=GLADSON_API_KEY,
        help="api_key: Gladson API license key"
    )


@pytest.fixture
def replication(request):
    credentials = (
        request.config.getoption("--user"), request.config.getoption("--password"),
        request.config.getoption("--user_token"), request.config.getoption("--api_key")
    )
    if None in credentials:
        raise EnvironmentError('\n'.join((
            'In order to perform unit tests for this module, please either set the following environment variables ' +
            'to reflect credentials for a current Gladson API account:',
            '    - "GLADSON_LOGIN" (in the format "user:password")',
            '    - "GLADSON_USER_TOKEN"',
            '    - "GLADSON_API_KEY"',
            '...or run pytest with (all) the following command line options:',
            '    --user',
            '    --password',
            '    --user_token',
            '    --api_key'
        )))
    user, password, user_token, api_key = credentials
    return Replication(
        user=user,
        password=password,
        user_token=user_token,
        api_key=api_key,
        echo=True
    )
