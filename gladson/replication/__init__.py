import functools
import json
import ssl
import sys
import traceback
from collections import OrderedDict
from datetime import date, datetime
from http.client import HTTPResponse
from http.cookiejar import CookieJar
from json import JSONDecodeError
from typing import Iterator
from typing import Optional
from urllib.error import HTTPError
from urllib.parse import urlencode
from urllib.request import Request, build_opener, HTTPCookieProcessor, HTTPSHandler, urlopen
from warnings import warn

from gladson import HOST
from gladson.replication.response import Asset, LoginResponse, LogoutResponse, JSONArray, ProductDetails,\
    ProductSearchResult
from gladson.replication.errors import STATUS_CODES_EXCEPTIONS, GladsonReplicationAPIResponseError, NotAuthorized, \
    InvalidCredentials, InternalSystemError, InvalidTokenOrLicense, SessionTimedOut

ORIGIN = 'https://%s/Replication/' % HOST


class Replication:

    def __init__(
        self,
        user,  # type: str
        password,  # type: str
        user_token,  # type: str
        api_key,  # type: str
        echo=False  # type: bool
    ):
        self.authorize = None  # type: Optional[Authorize]
        # self.cookie_jar = CookieJar()  # type: Optional[CookieJar]
        # self.opener = build_opener(  # type: Optional[OpenerDirector]
        #     HTTPCookieProcessor(self.cookie_jar),
        #     HTTPSHandler(
        #         # context=ssl._create_unverified_context()
        #         context=ssl.create_default_context()
        #     )
        # )
        self.authorize = Authorize(self, user, password, user_token, api_key)  # type: Authorize
        self.product = Product(self)  # type: Product
        self.echo = echo

    def __hash__(self):
        return hash((
            '' if self.authorize is None else self.authorize.user,
            '' if self.authorize is None else self.authorize.password,
            '' if self.authorize is None else self.authorize.user_token
        ))

    def request(
        self,
        service=None,  # type: str
        path=None,  # type: Optional[str]
        parameters=None,  # type: Optional[Union[Dict, str, bytes]],
        method='GET',  # type: str
        echo=False  # type: bool
    ):
        # type: (...) -> HTTPResponse
        if hasattr(parameters, 'items'):
            parameters = urlencode(
                parameters,
                # safe='+=/' if path == 'Logout' else '',  # +=
                # quote_via=quote
            )
        url = ORIGIN + service
        if path:
            url += ('' if path[0] == '/' else '/') + path
        if parameters and method == 'GET':
            url += '?' + parameters
        headers = {}
        if (self.authorize is not None) and (
            service != 'Authorize' or
            path != 'Login'
        ):
            headers['Authorization'] = 'bearer ' + self.authorize.access_token
        request = Request(
            url,
            headers=headers,
            method=method,
            **(
                {} if method == 'GET'
                else dict(
                    data=bytes(parameters, encoding='utf-8')
                )
            )
        )

        def request_text():
            return (
                ('%s: %s\n' % (request.get_method(), url)) +
                '\n'.join(
                    '%s: %s' % (k, v)
                    for k, v in request.header_items()
                ) + (
                    ('\n' + str(request.data, encoding='utf-8'))
                    if request.data is not None
                    else ''
                )
            ).strip()

        # if echo or self.echo:
        print(request_text())
        for i in range(2):
            try:
                response = urlopen(request)
            except HTTPError as e:
                response_text = ''
                response_data = None
                if hasattr(e, 'file') and hasattr(e.file, 'read'):
                    response_text = str(e.file.read(), 'utf-8').strip()
                    if response_text:
                        try:
                            response_data = json.loads(response_text)
                        except JSONDecodeError:
                            pass
                m = (
                    'Request:\n' +
                    request_text() +
                    '\n\nResponse:\n' +
                    response_text
                )
                et = None
                if (
                    (response_data is not None) and
                    (isinstance(response_data, dict)) and
                    ('StatusCode' in response_data) and
                    (response_data['StatusCode'] in STATUS_CODES_EXCEPTIONS)
                ):
                    et = STATUS_CODES_EXCEPTIONS[response_data['StatusCode']]
                    if (
                        (et is SessionTimedOut) or
                        (et is InvalidTokenOrLicense) or
                        (et is InvalidCredentials)
                    ):
                        if i == 0:
                            #if echo:
                            warn(
                                m + '\n' +
                                ''.join(traceback.format_exception(
                                    *sys.exc_info()
                                ))
                            )
                            self.authorize.login(echo=echo)
                            request.add_header(
                                'Authorization',
                                'bearer ' + self.authorize.access_token
                            )
                            continue
                        else:
                            m += (
                                '\n\nUser: %s' % self.authorize.user +
                                '\nPassword: %s' % self.authorize.password +
                                '\nUser Token: %s' % self.authorize.user_token +
                                '\nAccess Token: %s' % self.authorize.access_token
                            )
                if e.msg:
                    m += '\n\n' + e.msg
                if et is None:
                    e.msg = m
                else:
                    e = et(m)
                raise e
        return response


class Authorize:

    def __init__(
        self,
        replication,  # type: Replication
        user,  # type: str
        password,  # type: str
        user_token,  # type: str
        api_key  # type: str
    ):
        self.replication = replication
        self.user = user  # type: str
        self.password = password  # type: str
        self.user_token = user_token  # type: str
        self.api_key = api_key  # type: str
        self.service = 'Authorize'
        self._access_token = ''  # type: str
        self.expires = datetime.now()  # type: datetime

    @property
    def access_token(self):
        if datetime.now() >= self.expires:
            self.login()
        return self._access_token

    def login(
        self,
        # universal parameters
        uuid=None,  # type: Optional[str]
        os=None,  # type: Optional[str]
        latitude=None,  # type: Optional[str]
        longitude=None,  # type: Optional[str]
        first_name=None,  # type: Optional[str]
        last_name=None,  # type: Optional[str]
        gender=None,  # type: Optional[str]
        age=None,  # type: Optional[str]
        email_address=None,  # type: Optional[str]
        echo=False  # type: bool
    ):
        # type: (...) -> LoginResponse
        """
        :param uuid:

            The user ID of the party initiating this request.
            Optional—used for logging purposes only.

        :param os:

            The operating system used by the party initiating the request.
            Optional—used for logging purposes only.

        :param latitude:

            The latitude of the party initiating the request.
            Optional—used for logging purposes only.

        :param longitude:

            The longitude of the party initiating the request.

        :param first_name:

            The first name of the party initiating the request.
            Optional—used for logging purposes only.

        :param last_name:

            The last name of the party initiating the request.
            Optional—used for logging purposes only.

        :param gender:

            The gender of the party initiating the request.
            Optional—used for logging purposes only.

        :param age:

            The age of the party initiating the request.
            Optional—used for logging purposes only.

        :param email_address:

            The email ddress of the party initiating the request.
            Optional—used for logging purposes only.

        :param echo:
        :return:
        """
        parameters = OrderedDict([
            ('username', self.user),
            ('password', self.password + self.user_token),
            ('apikey', self.api_key)
        ])
        # universal parameters
        if uuid is not None:
            parameters['uuid'] = uuid
        if os is not None:
            parameters['os'] = os
        if latitude is not None:
            parameters['latitude'] = latitude
        if longitude is not None:
            parameters['longitude'] = longitude
        if first_name is not None:
            parameters['firstname'] = first_name
        if last_name is not None:
            parameters['lastname'] = last_name
        if gender is not None:
            parameters['gender'] = gender
        if age is not None:
            parameters['age'] = age
        if email_address is not None:
            parameters['emailaddress'] = email_address
        response = self.replication.request(
            self.service,
            'Login',
            parameters,
            method='POST',
            echo=echo
        )
        login_response = LoginResponse(str(response.read(), 'utf-8'))
        self._access_token = login_response.access_token
        self.expires = datetime.fromtimestamp(login_response.expires_at_unix_epoch)
        return login_response

    def logout(
        self,
        all_sessions=False,  # type: bool
        # universal parameters
        uuid=None,  # type: Optional[str]
        os=None,  # type: Optional[str]
        latitude=None,  # type: Optional[str]
        longitude=None,  # type: Optional[str]
        first_name=None,  # type: Optional[str]
        last_name=None,  # type: Optional[str]
        gender=None,  # type: Optional[str]
        age=None,  # type: Optional[str]
        email_address=None,  # type: Optional[str]
        echo=False  # type: bool
    ):
        # type: (...) -> int
        """

        :param all_sessions:

            If `True`, all sessions initiated by this user will be logged out. Otherwise, only
            this session will be ended.

        :param uuid:

            The user ID of the party initiating this request.
            Optional—used for logging purposes only.

        :param os:

            The operating system used by the party initiating the request.
            Optional—used for logging purposes only.

        :param latitude:

            The latitude of the party initiating the request.
            Optional—used for logging purposes only.

        :param longitude:

            The longitude of the party initiating the request.

        :param first_name:

            The first name of the party initiating the request.
            Optional—used for logging purposes only.

        :param last_name:

            The last name of the party initiating the request.
            Optional—used for logging purposes only.

        :param gender:

            The gender of the party initiating the request.
            Optional—used for logging purposes only.

        :param age:

            The age of the party initiating the request.
            Optional—used for logging purposes only.

        :param email_address:

            The email ddress of the party initiating the request.
            Optional—used for logging purposes only.

        :param echo:

            If `True`, debugging information will be printed to the system output.

        :return:

            The HTTP response code (200 = OK).
        """
        parameters = {} #[('token', self.access_token)])
        if all_sessions:
            parameters['username'] = self.user
        else:
            parameters['token'] = self.access_token
        # universal parameters
        if uuid is not None:
            parameters['uuid'] = uuid
        if os is not None:
            parameters['os'] = os
        if latitude is not None:
            parameters['latitude'] = latitude
        if longitude is not None:
            parameters['longitude'] = longitude
        if first_name is not None:
            parameters['firstname'] = first_name
        if last_name is not None:
            parameters['lastname'] = last_name
        if gender is not None:
            parameters['gender'] = gender
        if age is not None:
            parameters['age'] = age
        if email_address is not None:
            parameters['emailaddress'] = email_address
        response = self.replication.request(
            self.service,
            'Logout',
            parameters,
            method='POST',
            echo=echo
        )
        return json.loads(str(response.read(), 'utf-8'))


class Product:

    def __init__(
        self,
        replication  # type: Replication
    ):
        self.replication = replication
        self.service = 'Product'

    def search(
        self,
        upc=None,  # type: Optional[Union[str, int]]
        brand=None,  # type: Optional[str]
        on_hold=None,  # type: Optional[bool]
        language=None,  # type: Optional[str]
        description=None,  # type: Optional[str]
        start_date=None,  # type: Optional[date]
        end_date=None,  # type: Optional[date]
        category=None,  # type: Optional[str]
        # universal parameters
        uuid=None,  # type: Optional[str]
        os=None,  # type: Optional[str]
        latitude=None,  # type: Optional[str]
        longitude=None,  # type: Optional[str]
        first_name=None,  # type: Optional[str]
        last_name=None,  # type: Optional[str]
        gender=None,  # type: Optional[str]
        age=None,  # type: Optional[str]
        email_address=None,  # type: Optional[str]
        echo=None  # type: bool
    ):
        # type: (...) -> Iterator[ProductSearchResult]
        """

        :param upc:

            A universal product code.

        :param brand:

            A brand name.

        :param on_hold:

            If `True`, search results include items which manufacturers have flagged
            as not being currently on the market.
            This parameter defaults to `False`.

        :param language:

            A language code (or country-dependent language code) indicating the language
            in which to return attribute values.
            This parameter defaults to "US-ENG".

        :param description:

            Keywords to search for in the product description.

        :param start_date:

            All products which were last updated prior to this date will be excluded from returned results.
            Note: At the time this was written, the earliest "receipt date" in Gladson's database was 1999-11-01.

        :param end_date:

            All products which were last updated after this date will be excluded from returned results.
            This parameter defaults to today's date.

        :param category:

            If provided, only items with the specified category name are returned.

        :param uuid:

            The user ID of the party initiating this request.
            Optional—used for logging purposes only.

        :param os:

            The operating system used by the party initiating the request.
            Optional—used for logging purposes only.

        :param latitude:

            The latitude of the party initiating the request.
            Optional—used for logging purposes only.

        :param longitude:

            The longitude of the party initiating the request.

        :param first_name:

            The first name of the party initiating the request.
            Optional—used for logging purposes only.

        :param last_name:

            The last name of the party initiating the request.
            Optional—used for logging purposes only.

        :param gender:

            The gender of the party initiating the request.
            Optional—used for logging purposes only.

        :param age:

            The age of the party initiating the request.
            Optional—used for logging purposes only.

        :param email_address:

            The email ddress of the party initiating the request.
            Optional—used for logging purposes only.

        :return:

            A list of results matching the supplied parameters.
        """
        parameters = OrderedDict()
        if upc is not None:
            parameters['upc'] = upc
        if brand is not None:
            parameters['brand'] = brand
        if language is not None:
            parameters['language'] = language
        if description is not None:
            parameters['description'] = description
        if start_date is not None:
            if isinstance(start_date, (date, datetime)):
                parameters['startdate'] = start_date.strftime('%Y-%m-%d')
            elif isinstance(start_date, str):
                parameters['startdate'] = start_date
        if end_date is not None:
            if isinstance(end_date, (date, datetime)):
                parameters['enddate'] = end_date.strftime('%Y-%m-%d')
            elif isinstance(end_date, str):
                parameters['enddate'] = end_date
        if category is not None:
            parameters['category'] = category
        if on_hold is not None:
            parameters['onhold'] = 'true' if on_hold else 'false'
        # universal parameters
        if uuid is not None:
            parameters['uuid'] = uuid
        if os is not None:
            parameters['os'] = os
        if latitude is not None:
            parameters['latitude'] = latitude
        if longitude is not None:
            parameters['longitude'] = longitude
        if first_name is not None:
            parameters['firstname'] = first_name
        if last_name is not None:
            parameters['lastname'] = last_name
        if gender is not None:
            parameters['gender'] = gender
        if age is not None:
            parameters['age'] = age
        if email_address is not None:
            parameters['emailaddress'] = email_address
        response = self.replication.request(
            self.service,
            'Search',
            parameters,
            method='GET',
            echo=echo
        )
        response_data = str(response.read(), 'utf-8')
        results = json.loads(response_data)
        return JSONArray(
            [ProductSearchResult(r) for r in results],
            response=response
        )

    @functools.lru_cache(maxsize=32)
    def details(
        self,
        upc,  # type: str
        modifier=None,  # type: Optional[str]
        brand=None,  # type: Optional[str]
        # universal parameters
        uuid=None,  # type: Optional[str]
        os=None,  # type: Optional[str]
        latitude=None,  # type: Optional[str]
        longitude=None,  # type: Optional[str]
        first_name=None,  # type: Optional[str]
        last_name=None,  # type: Optional[str]
        gender=None,  # type: Optional[str]
        age=None,  # type: Optional[str]
        email_address=None,  # type: Optional[str]
        echo=False  # type: bool
    ):
        # type: (...) -> ProductDetails
        """

        :param upc:

            The UPC (also known as GTIN/EAN/ISBN) of an item.

        :param modifier:

            A single-digit modifier (A-Z) identifying a variant when multiple distinct items share
            the same UPC.

        :param brand:

            The brand name of the item. Note: Some items with the same UPC are branded differently in different regions,
            hence the need to be able to differentiate.

        :param uuid:

            The user ID of the party initiating this request.
            Optional—used for logging purposes only.

        :param os:

            The operating system used by the party initiating the request.
            Optional—used for logging purposes only.

        :param latitude:

            The latitude of the party initiating the request.
            Optional—used for logging purposes only.

        :param longitude:

            The longitude of the party initiating the request.

        :param first_name:

            The first name of the party initiating the request.
            Optional—used for logging purposes only.

        :param last_name:

            The last name of the party initiating the request.
            Optional—used for logging purposes only.

        :param gender:

            The gender of the party initiating the request.
            Optional—used for logging purposes only.

        :param age:

            The age of the party initiating the request.
            Optional—used for logging purposes only.

        :param email_address:

            The email ddress of the party initiating the request.
            Optional—used for logging purposes only.

        :param echo:
        :return:
        """
        parameters = OrderedDict([
            ('upc', upc)
        ])
        if brand:
            parameters['brand'] = brand
        if modifier is not None:
            parameters['modifier'] = modifier
        # universal parameters
        if uuid is not None:
            parameters['uuid'] = uuid
        if os is not None:
            parameters['os'] = os
        if latitude is not None:
            parameters['latitude'] = latitude
        if longitude is not None:
            parameters['longitude'] = longitude
        if first_name is not None:
            parameters['firstname'] = first_name
        if last_name is not None:
            parameters['lastname'] = last_name
        if gender is not None:
            parameters['gender'] = gender
        if age is not None:
            parameters['age'] = age
        if email_address is not None:
            parameters['emailaddress'] = email_address
        response = self.replication.request(
            self.service,
            path=None,
            parameters=parameters,
            method='GET',
            echo=echo or False
        )
        response_data = str(response.read(), 'utf-8')
        return ProductDetails(
            response_data
        )

    def asset(
        self,
        upc,  # type: str
        modifier=None,  # type: Optional[str]
        brand=None,  # type: Optional[str]
        on_hold=None,  # type: Optional[bool]
        asset_type=None,  # type: Optional[str]
        asset_sub_type=None,  # type: Optional[str]
        quality=None,  # type: Optional[Union[str, int]]
        file_format=None,  # type: Optional[str]
        # universal parameters
        uuid=None,  # type: Optional[str]
        os=None,  # type: Optional[str]
        latitude=None,  # type: Optional[str]
        longitude=None,  # type: Optional[str]
        first_name=None,  # type: Optional[str]
        last_name=None,  # type: Optional[str]
        gender=None,  # type: Optional[str]
        age=None,  # type: Optional[str]
        email_address=None,  # type: Optional[str]
        echo=False  # type: bool
    ):
        # type: (...) -> HTTPResponse
        """
        :param upc:

            The UPC (also known as GTIN/EAN/ISBN) of an item.

        :param modifier:

            A single-digit modifier (A-Z) identifying a variant when multiple distinct items share
            the same UPC.

        :param brand:

            The brand name of the item.

        :param on_hold:

            If "True"—returns products which have been flagged as not currently being on the market.
            Defaults to "False".

        :param asset_type:

            The type of asset: "image", "video", "document", or "other".

        :param asset_sub_type:

            A string or integer describing the quality in which the asset is available. For images, this is an integer
            representing the number of pixels along the longest side of the image. For example: "1000" or "500".

        :param quality:

            The quality of the asset. For images: The number of pixels on the longest side.

        :param file_format:

             The file format/extension of the asset. For example: "Jpeg" or "Png".

        :param uuid:

            The user ID of the party initiating this request.
            Optional—used for logging purposes only.

        :param os:

            The operating system used by the party initiating the request.
            Optional—used for logging purposes only.

        :param latitude:

            The latitude of the party initiating the request.
            Optional—used for logging purposes only.

        :param longitude:

            The longitude of the party initiating the request.

        :param first_name:

            The first name of the party initiating the request.
            Optional—used for logging purposes only.

        :param last_name:

            The last name of the party initiating the request.
            Optional—used for logging purposes only.

        :param gender:

            The gender of the party initiating the request.
            Optional—used for logging purposes only.

        :param age:

            The age of the party initiating the request.
            Optional—used for logging purposes only.

        :param email_address:

            The email ddress of the party initiating the request.
            Optional—used for logging purposes only.

        :param echo:
        :return:
        """
        parameters = OrderedDict([
            ('upc', upc)
        ])
        if brand is not None:
            parameters['brand'] = brand
        if brand is not None:
            parameters['brand'] = brand
        if modifier is not None:
            parameters['modifier'] = modifier
        if asset_type is not None:
            parameters['assettype'] = asset_type
        if asset_sub_type is not None:
            parameters['assetsubtype'] = asset_sub_type
        if quality is not None:
            parameters['quality'] = str(quality)
        if file_format is not None:
            parameters['fileformat'] = file_format
        if on_hold is not None:
            parameters['onhold'] = 'true' if on_hold else 'false'
        # universal parameters
        if uuid is not None:
            parameters['uuid'] = uuid
        if os is not None:
            parameters['os'] = os
        if latitude is not None:
            parameters['latitude'] = latitude
        if longitude is not None:
            parameters['longitude'] = longitude
        if first_name is not None:
            parameters['firstname'] = first_name
        if last_name is not None:
            parameters['lastname'] = last_name
        if gender is not None:
            parameters['gender'] = gender
        if age is not None:
            parameters['age'] = age
        if email_address is not None:
            parameters['emailaddress'] = email_address
        return self.replication.request(
            self.service,
            path='Asset',
            parameters=parameters,
            method='GET',
            echo=echo
        )

    def assets(
        self,
        upc,  # type: str
        modifier=None,  # type: Optional[str]
        brand=None,  # type: Optional[str]
        # universal parameters
        uuid=None,  # type: Optional[str]
        os=None,  # type: Optional[str]
        latitude=None,  # type: Optional[str]
        longitude=None,  # type: Optional[str]
        first_name=None,  # type: Optional[str]
        last_name=None,  # type: Optional[str]
        gender=None,  # type: Optional[str]
        age=None,  # type: Optional[str]
        email_address=None,  # type: Optional[str]
        echo=False,  # type: bool
    ):
        # type: (...) -> Iterator[HTTPResponse]
        """
        :param upc:

            The UPC (also known as GTIN/EAN/ISBN) of an item.

        :param modifier:

            A single-digit modifier (A-Z) identifying a variant when multiple distinct items share
            the same UPC.

        :param brand:

            The brand name of the item.

        :param uuid:

            The user ID of the party initiating this request.
            Optional—used for logging purposes only.

        :param os:

            The operating system used by the party initiating the request.
            Optional—used for logging purposes only.

        :param latitude:

            The latitude of the party initiating the request.
            Optional—used for logging purposes only.

        :param longitude:

            The longitude of the party initiating the request.

        :param first_name:

            The first name of the party initiating the request.
            Optional—used for logging purposes only.

        :param last_name:

            The last name of the party initiating the request.
            Optional—used for logging purposes only.

        :param gender:

            The gender of the party initiating the request.
            Optional—used for logging purposes only.

        :param age:

            The age of the party initiating the request.
            Optional—used for logging purposes only.

        :param email_address:

            The email ddress of the party initiating the request.
            Optional—used for logging purposes only.

        :param echo:
        :return:
        """
        details = self.details(  # type: ProductDetails
            upc,
            modifier=modifier,
            brand=brand,
            uuid=uuid,
            os=os,
            latitude=latitude,
            longitude=longitude,
            first_name=first_name,
            last_name=last_name,
            gender=gender,
            age=age,
            email_address=email_address,
            echo=echo
        )
        for a in (details.assets or []):  # type: Asset
            try:
                yield self.asset(
                    upc=upc,
                    modifier=modifier or None,
                    brand=details.brand,
                    asset_type=a.asset_type,
                    asset_sub_type=a.asset_sub_type,
                    file_format=a.file_format,
                    quality=a.max_quality,
                    uuid=uuid,
                    os=os,
                    latitude=latitude,
                    longitude=longitude,
                    first_name=first_name,
                    last_name=last_name,
                    gender=gender,
                    age=age,
                    email_address=email_address,
                    echo=echo
                )
            except NotAuthorized:
                yield None


if __name__ == "__main__":
    import doctest
    doctest.testmod()
