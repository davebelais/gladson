from copy import copy


class GladsonReplicationAPIError(Exception):

    pass


class GladsonReplicationAPIWarning(Warning):

    pass


class GladsonReplicationAPIParsingError(GladsonReplicationAPIError):

    pass


class GladsonReplicationAPIKeyError(KeyError, GladsonReplicationAPIParsingError):

    pass


class GladsonReplicationAPIKeyWarning(Warning):

    pass


class GladsonReplicationAPIResponseError(GladsonReplicationAPIError):

    status_code = ''  # type: str
    description = ''  # type: str

    def __init__(
        self,
        message=None  # type: Optional[str]
    ):
        super().__init__(
            '%s: %s\n' % (
                self.status_code,
                self.description
            ) + (
                '' if message is None
                else '\n' + message
            )
        )


class InvalidCredentials(GladsonReplicationAPIResponseError):

    status_code = 'GA400'  # type: str
    description = 'Invalid credentials entered'  # type: str


class AccountLocked(GladsonReplicationAPIResponseError):

    status_code = 'GA401'  # type: str
    description = 'Account locked'  # type: str


class NotLicensed(GladsonReplicationAPIResponseError):

    status_code = 'GA402'  # type: str
    description = 'The caller has no license access to the product or image size requested'  # type: str


class SessionTimedOut(GladsonReplicationAPIResponseError):

    status_code = 'GA403'  # type: str
    description = 'Session timed out'  # type: str


class InvalidTokenOrLicense(GladsonReplicationAPIResponseError):

    status_code = 'GO411'  # type: str
    description = 'Invalid API token or invalid license'  # type: str


class LicenseQuantityLimitReached(GladsonReplicationAPIResponseError):

    status_code = 'GO413'  # type: str
    description = 'License quantity limit reached'  # type: str


class ProductNotFound(GladsonReplicationAPIResponseError):

    status_code = 'GO421'  # type: str
    description = 'Product not found'  # type: str


class UserQuantityLimitReached(GladsonReplicationAPIResponseError):

    status_code = 'GO423'  # type: str
    description = 'User quantity limit reached'  # type: str


class NotAuthorized(GladsonReplicationAPIResponseError):

    status_code = 'GO431'  # type: str
    description = 'Not authorized to access asset requested'  # type: str


class LicenseSearchLimitReached(GladsonReplicationAPIResponseError):

    status_code = 'GO433'  # type: str
    description = 'License search limit reached'  # type: str


class InternalSystemError(GladsonReplicationAPIResponseError):

    status_code = 'GC500'  # type: str
    description = 'Internal System error'  # type: str


# A map of status codes to their corresponding exceptions


STATUS_CODES_EXCEPTIONS = {}

for k, v in copy(locals()).items():
    if (
        k[0] != '_' and
        isinstance(v, type) and
        hasattr(v, 'status_code') and
        hasattr(v, 'description')
    ):
        status_code = getattr(v, 'status_code')  # type: int
        STATUS_CODES_EXCEPTIONS[status_code] = v
